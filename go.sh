#!/bin/bash

image_prefix=kaig2008
image_repo=alpine-openjdk8
image_tag=0.1

op="$*"

# if op=="" set op="-r -b"

if [ -z $op ] ;then
 op="-rb"
fi

sh build.sh "$image_prefix" "$image_repo" "$image_tag" "$op"
