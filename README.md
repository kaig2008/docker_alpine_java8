# docker_alpine_java8

#### 介绍
alpine-openjdk8 docker镜像
#alpine:3.12
#openjdk:jdk8u265-b01
FROM adoptopenjdk/openjdk8:alpine-slim



#### 软件架构
软件架构说明


#### 使用说明

前提：
已经安装好docker环境

## 打docker images
```
./go.sh -b
```

## 通过创建的镜像启动容器
```
./go.sh -r
```

## 打包并启动容器
```
./go.sh -br 或 ./go.sh
```

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
