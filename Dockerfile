#alpine:3.12
#openjdk:jdk8u265-b01
#FROM adoptopenjdk/openjdk8:alpine
FROM adoptopenjdk/openjdk8:alpine-slim

#Author
#MAINTAINER kaig2008@126.com
LABEL maintainer="kaig2008@126.com"

#******************更换Alpine源为mirrors.aliyun.com******************
RUN echo http://mirrors.aliyun.com/alpine/v3.10/main/ > /etc/apk/repositories && \
    echo http://mirrors.aliyun.com/alpine/v3.10/community/ >> /etc/apk/repositories
RUN apk update && apk upgrade

#*****时区、时间调整******************
RUN apk add tzdata && cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
&& echo "Asia/Shanghai" > /etc/timezone \
&& apk del tzdata

RUN date >/home/tmp.txt

CMD ["java","-version"]

